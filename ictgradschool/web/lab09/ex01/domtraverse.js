"use strict";

/* Your answer here */
var divs= document.getElementsByTagName ("div");
var divOne = divs[0];
divOne.style.color = "pink";

var paragraphsInLastDiv = document.querySelectorAll ( ".text3 p");
var lastParagraph =  paragraphsInLastDiv [paragraphsInLastDiv.length-1];
lastParagraph.style.color = "red";

var divTwo = divs[1];
var theParentOfDivTwo = divTwo.parentNode;
theParentOfDivTwo.removeChild (divTwo);

var firstParagraph = document.getElementsByTagName("p")[0];
firstParagraph.innerText = "Hello World!";

var button = document.getElementById ("makeBold");
button.onclick = function () {
    var allParagraphs =document.getElementsByTagName("p");
    for (var i =0; i< allParagraphs.length; i++) {
        allParagraphs[i].style.fontWeight= "bold";
    }
};