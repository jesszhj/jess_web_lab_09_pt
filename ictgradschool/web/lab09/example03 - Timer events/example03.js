
// This function will be called as a result of a timer "timeout".
function timeoutFunction() {

    console.log("Timeout!");
    
    var container = document.getElementById("timeoutContainer");
    container.innerHTML +="<p>Timeout!</p>";
}

// This function will be called as a result of a timer "interval".
function intervalFunction() {

    console.log("Interval!");

    var container = document.getElementById("intervalContainer");
    container.innerHTML +="<p>Interval!</p>";
}

// This variable will store a reference to the interval timer, so we can stop it later once we've started it.
// The variable starts off as null (which means "no value") because we won't actually start the interval timer until
// the button is clicked.
var intervalTimer = null;

// This function (which will be automatically called when the page loads) will set up some event handlers.
window.onload = function() {

    // Add an event handler to the "Start Timeout" button which will set a timer timeout.
    var startTimeoutButton = document.getElementById("startTimeout");
    startTimeoutButton.onclick = function () {

        // The setTimeout function will take two arguments: a function and a value in milliseconds.
        // This will cause the given function to be executed after the given amount of time.
        // In this case, "timeoutFunction" will be called once, after a 2000ms (2 second) delay.
        setTimeout(timeoutFunction, 2000);
    };

    // Add an event handler to the "Start Interval" button which will set a timer interval.
    var startIntervalButton = document.getElementById("startInterval");
    startIntervalButton.onclick = function () {

        // If we haven't already started the timer...
        if (intervalTimer == null) {

            // The setInterval function will take two arguments: a function and a value in milliseconds.
            // This will cause the given function to be called repeatedly, with the given delay in between each call.
            // In this case, "intervalFunction" will be called repeatedly, every 1000ms (1 second).
            // This function also returns a value which we can use to stop the intervals occuring later (see below).
            intervalTimer = setInterval(intervalFunction, 1000);

        }

        // If we already started the timer, we don't want to call setInterval again - this will actually cause two timers
        // to be running (comment out all the "if" statement logic for a visualization of this if you like).
        // In some cases, that may be what you want, but in this case it certainly isn't.
        else {
            alert("Timer already started!");
        }
    };

    // Add an event handler to the "Stop Interval" button which will clear a timer interval.
    var stopIntervalButton = document.getElementById("stopInterval");
    stopIntervalButton.onclick = function () {

        // Good practice to check somehow if the timer is running before trying to stop it.
        if (intervalTimer != null) {

            // The clearInterval function will stop the given timer (so its interval function will stop being
            // repeatedly called).
            clearInterval(intervalTimer);

            intervalTimer = null;

        }

        else {
            alert("Timer hasn't started. Please start it first.");
        }
    };

}